![Logo of the project](./Logo.png)

## Project Ruby On Rails without login

Example of a Ruby On Rails project without login


## Technology 

Here are the technologies used in this project.

* Docker version 20.10.5, build 55c4c88 (https://www.docker.com/)
* PostgreSQL version 9.6.7 Alpine (Docker Image: https://hub.docker.com/_/postgres/)
* Ruby version 2.7.1 (https://www.ruby-lang.org/pt/)
* Ruby On Rails version 6.0.3.6 (https://rubyonrails.org/)
* Nginx version 1.18 (https://nginx.org/en/)
* NodeJS version 12 (https://nodejs.org/en/)
* Puma version 3.12.6 (https://puma.io/)
* Bootstrap version 5.1.0 (https://startbootstrap.com/)
* Bootstrap Icons version 1.5.0 (https://icons.getbootstrap.com/)
* Google fonts (https://fonts.google.com/)
* Lightbox2 (https://lokeshdhakar.com/projects/lightbox2/)
* Font Awesome version 5.10.0(https://fontawesome.com/)


## Services Used

* Dockerhub (https://hub.docker.com/)


## Getting started

* To start, you need to have docker and Ruby on Rails installed


## How to use
* Access the project name folder and run the command to create the Ruby On Rails project:
>   $ docker-compose run app rails new . -d postgresql -T
    
* Build de project:
>    $ docker-compose build

* In the file "database.yml" add to the method "default: &default", data for connection to postgres:
    ```
    username: <%= ENV.fetch('POSTGRES_USER') %>
    password: <%= ENV.fetch('POSTGRES_PASSWORD') %>
    host: <%= ENV.fetch('POSTGRES_HOST') %>
    ```
    
* In the file /NAME_RAILS_PROJECT/.gitignore overwrite the lines 11-14 to:
    ```
    /log/*
    /tmp/*
    /tmp/pids/*              # this line
    !/log/.keep
    !/tmp/.keep
    !/tmp/pids               # this line
    !/tmp/pids/.keep         # and this line
    ```

* Add manualy the folder "pids/" in folder "tmp/" to version control using a .keep file.
    ```
        ...
        tmp
           |_> pids
                 |_> .keep
        ...
    ```

* Create the file "app/assets/stylesheets/lightbox.css" to have the Lightbox effect on the images

* Create the file "app/javascript/packs/lightbox-plus-jquery.js" to have the Lightbox effect on the images

* Add the files "lightbox-plus-jquery.js and lightbox.css" to be loaded by Rails, add this line in the file "config/initializers/assets.rb":
    ```
    Rails.application.config.assets.precompile += %w( lightbox-plus-jquery.js lightbox.css )
    ```
    
* Add in the tag <head> importing LightBox CSS file, Favicon file, Bootstrap Icons CDN, Google Fonts CDN, Font Awesome CDN in the "app/views/layouts/application.html.erb":
    ```
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
    <!-- Bootstrap Icons-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic" rel="stylesheet" type="text/css" />
    <!-- Lightbox CSS-->
    <%= stylesheet_link_tag 'lightbox.css' %>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />
    ```

* Add in the end of tag <body> importing LightBox Javascript file, Bootstrap in the Javascript CDN in the "app/views/layouts/application.html.erb"::
    ```
    <!-- Bootstrap core JS-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Lightbox JS-->
    <%= javascript_pack_tag 'lightbox-plus-jquery.js' %>
    <!-- Core theme JS-->
    <%= javascript_pack_tag 'scripts.js' %>
    ```

* Add the icons (close.png, loading.gif, next.png, prev.png) of LightBox in the "app/assets/images"

* Creatin Home Site
>    $ docker-compose run --rm app rails g controller Page home

* Creating default route to home (overwrite the content of "config/routes.rb" to):
    ```
    root  to: 'page#home'
    ```

* Add the CSS layout styles in the file "app/assets/stylesheets/page.scss"

* Change file extension "/app/assets/stylesheets/application.css" to "/app/assets/stylesheets/application.scss" and add CSS layout file import in the "/app/assets/stylesheets/application.scss", overwrite all to:
    ```
    @import "page";
    ```

* Add the images (bg-masterhead.jpg, favicon.ico, folder portifolio) from the website in the "app/assets/images"

* Add to the file "app/javascript/packs/scripts.js" to be loaded by Rails, overwrite the line "Rails.application.config.assets.precompile" in the file "config/initializers/assets.rb" to:
    ```
    Rails.application.config.assets.precompile += %w( scripts.js lightbox-plus-jquery.js lightbox.css )
    ```

* Add the javascript encodings of the layout in the file "app/javascript/packs/scripts.js"

* Overwrite the content of "app/views/page/home.html.erb" to the content of site

* Create the file "app/views/page/_about.html.erb" with content about

* Create the file "app/views/page/_contact.html.erb" with content contact

* Create the file "app/views/page/_footer.html.erb" with content footer

* Create the file "app/views/page/_masthead.html.erb" with content masthead

* Create the file "app/views/page/_navbar.html.erb" with content navbar

* Create the file "app/views/page/_portfolio.html.erb" with content portfolio

* Create the file "app/views/page/_services.html.erb" with content services

* Create de BD of project
>   $ docker-compose run --rm app rails db:create

* To run the project:
>    $ docker-compose up

## Versioning of this project 

1.0.0


## Authors

* **Fernando Altir Bastian**: @Fernando-Altir-Bastian (https://gitlab.com/fernando.bastian)


Please follow gitlab and join us!
Thanks to visiting me and good coding!



